package com.example.demo.repo;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.Producto;

public interface IProductoRepo extends JpaRepository<Producto, Integer> {

    @Query(value = "select pro.* from producto pro inner join orden o on pro.id_orden = o.id_orden where fecha BETWEEN to_date(:fechaIni, 'yyyy-MM-dd') and to_date(:fechaFin, 'yyyy-MM-dd')", nativeQuery = true)
    List<Producto> buscarReporte(@Param("fechaIni") String fechaIni, @Param("fechaFin") String fechaFin);
}
