package com.example.demo.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.demo.model.Orden;

public interface IOrdenRepo extends JpaRepository<Orden, Integer> {

}
