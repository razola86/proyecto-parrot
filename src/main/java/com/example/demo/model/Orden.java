package com.example.demo.model;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.ForeignKey;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "orden")
public class Orden {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer idOrden;

    @ManyToOne
    @JoinColumn(name = "id_usuario", nullable = false, foreignKey = @ForeignKey(name = "FK_orden_usuario"))
    private Usuario usuario;

    @Column(name = "nombre_cliente", nullable = false, length = 70)
    private String nombreClinte;

    @Column(name = "total", nullable = false, precision = 10, scale = 2)
    private BigDecimal total;

    @Column(name = "fecha")
    private LocalDateTime fecha;

    @OneToMany(mappedBy = "orden", cascade = { CascadeType.ALL
    }, orphanRemoval = true)
    private List<Producto> producto;

    public Integer getIdOrden() {
        return idOrden;
    }

    public void setIdOrden(final Integer idOrden) {
        this.idOrden = idOrden;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(final Usuario usuario) {
        this.usuario = usuario;
    }

    public String getNombreClinte() {
        return nombreClinte;
    }

    public void setNombreClinte(final String nombreClinte) {
        this.nombreClinte = nombreClinte;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(final BigDecimal total) {
        this.total = total;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(final LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public List<Producto> getProducto() {
        return producto;
    }

    public void setProducto(final List<Producto> producto) {
        this.producto = producto;
    }

}
