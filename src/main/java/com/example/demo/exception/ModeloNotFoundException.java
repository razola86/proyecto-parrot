package com.example.demo.exception;

public class ModeloNotFoundException extends RuntimeException {

    private static final long serialVersionUID = 471326526544433369L;

    public ModeloNotFoundException(final String mensaje) {
        super(mensaje);
    }
}
