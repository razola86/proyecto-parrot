package com.example.demo.exception;

import java.time.LocalDateTime;

public class ExceptionResponse {

    private LocalDateTime fecha;
    private String mensaje;
    private String detalles;

    public ExceptionResponse(final LocalDateTime fechaHora, final String mensaje, final String detalles) {
        this.fecha = fechaHora;
        this.mensaje = mensaje;
        this.detalles = detalles;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(final LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(final String mensaje) {
        this.mensaje = mensaje;
    }

    public String getDetalles() {
        return detalles;
    }

    public void setDetalles(final String detalles) {
        this.detalles = detalles;
    }

}
