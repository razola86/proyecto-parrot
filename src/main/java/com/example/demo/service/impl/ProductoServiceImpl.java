package com.example.demo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Producto;
import com.example.demo.repo.IProductoRepo;
import com.example.demo.service.IProductoService;

@Service
public class ProductoServiceImpl implements IProductoService {

    @Autowired
    private IProductoRepo repo;

    @Override
    public Producto registrar(final Producto obj) {
        return repo.save(obj);
    }

    @Override
    public List<Producto> listar() {
        return repo.findAll();
    }

    @Override
    public Producto listarPorId(final Integer id) {
        final Optional<Producto> op = repo.findById(id);
        return op.isPresent() ? op.get() : new Producto();
    }

    @Override
    public List<Producto> buscarReporte(final String fechaIni, final String fechaFin) {
        return repo.buscarReporte(fechaIni, fechaFin);
    }

}
