package com.example.demo.service.impl;

import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Orden;
import com.example.demo.repo.IOrdenRepo;
import com.example.demo.service.IOrdenService;

@Service
public class OrdenServiceImpl implements IOrdenService {

    @Autowired
    private IOrdenRepo repo;

    @Transactional
    @Override
    public Orden registrar(final Orden obj) {
        obj.getProducto().forEach(det -> {
            det.setOrden(obj);
        });
        return repo.save(obj);
    }

    @Override
    public List<Orden> listar() {
        return repo.findAll();
    }

    @Override
    public Orden listarPorId(final Integer id) {
        final Optional<Orden> op = repo.findById(id);
        return op.isPresent() ? op.get() : new Orden();
    }

}
