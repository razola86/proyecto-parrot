package com.example.demo.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.model.Usuario;
import com.example.demo.repo.IUsuarioRepo;
import com.example.demo.service.IUsuarioService;

@Service
public class UsuarioServiceImpl implements IUsuarioService {

    @Autowired
    private IUsuarioRepo repo;

    @Override
    public Usuario registrar(final Usuario obj) {
        return repo.save(obj);
    }

    @Override
    public List<Usuario> listar() {
        return repo.findAll();
    }

    @Override
    public Usuario listarPorId(final Integer id) {
        final Optional<Usuario> op = repo.findById(id);
        return op.isPresent() ? op.get() : new Usuario();
    }

}
