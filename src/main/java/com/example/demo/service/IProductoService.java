package com.example.demo.service;

import java.util.List;

import com.example.demo.model.Producto;

public interface IProductoService extends ICRUD<Producto, Integer> {

    List<Producto> buscarReporte(String fechaIni, String fechaFin);
}
