package com.example.demo.controller;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.demo.dto.OrdenDTO;
import com.example.demo.dto.ProductoDTO;
import com.example.demo.exception.ModeloNotFoundException;
import com.example.demo.model.Orden;
import com.example.demo.service.IOrdenService;

@RestController
@RequestMapping("/ordenes")
public class OrdenController {

    @Autowired
    private IOrdenService ordenSrv;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity<List<OrdenDTO>> listar() {

        final List<OrdenDTO> lista = ordenSrv.listar().stream().map(user -> modelMapper.map(user, OrdenDTO.class))
                .collect(Collectors.toList());
        if (lista.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return new ResponseEntity<List<OrdenDTO>>(lista, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<OrdenDTO> registrar(@Valid @RequestBody final OrdenDTO objeto) {

        final List<ProductoDTO> pdto = objeto.getProducto().stream().collect(Collectors
                .collectingAndThen(Collectors.toMap(ProductoDTO::getNombre, Function.identity(), (left, right) -> {
                    left.setCantidad(left.getCantidad() + right.getCantidad());
                    return left;
                }), m -> new ArrayList<>(m.values())));
        objeto.setProducto(pdto);
        final OrdenDTO user = modelMapper.map(ordenSrv.registrar(modelMapper.map(objeto, Orden.class)), OrdenDTO.class);
        final URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(user.getIdOrden()).toUri();
        return ResponseEntity.created(location).build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<OrdenDTO> listarPorId(@PathVariable("id") final Integer id) {
        final OrdenDTO obj = modelMapper.map(ordenSrv.listarPorId(id), OrdenDTO.class);
        if (obj.getIdOrden() == null) {
            throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
        }
        return new ResponseEntity<OrdenDTO>(obj, HttpStatus.OK);
    }
}
