package com.example.demo.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.ProductoDTO;
import com.example.demo.exception.ModeloNotFoundException;
import com.example.demo.service.IProductoService;

@RestController
@RequestMapping("/productos")
public class ProductoController {

    @Autowired
    private IProductoService productoSrv;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity<List<ProductoDTO>> listar() {

        final List<ProductoDTO> lista = productoSrv.listar().stream()
                .map(user -> modelMapper.map(user, ProductoDTO.class)).collect(Collectors.toList());

        final List<ProductoDTO> pdto = lista.stream().collect(Collectors
                .collectingAndThen(Collectors.toMap(ProductoDTO::getNombre, Function.identity(), (left, right) -> {
                    left.setCantidad(left.getCantidad() + right.getCantidad());
                    return left;
                }), m -> new ArrayList<>(m.values())));
        return new ResponseEntity<List<ProductoDTO>>(pdto, HttpStatus.OK);
    }

    @GetMapping("/{id}")
    public ResponseEntity<ProductoDTO> listarPorId(@PathVariable("id") final Integer id) {
        final ProductoDTO obj = modelMapper.map(productoSrv.listarPorId(id), ProductoDTO.class);
        if (obj.getIdProducto() == null) {
            throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
        }
        return new ResponseEntity<ProductoDTO>(obj, HttpStatus.OK);
    }
}
