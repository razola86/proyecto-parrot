package com.example.demo.controller;

import java.net.URI;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.example.demo.dto.UsuarioDTO;
import com.example.demo.exception.ModeloNotFoundException;
import com.example.demo.model.Usuario;
import com.example.demo.service.IUsuarioService;

@RestController
@RequestMapping("/usuarios")
public class UsuarioController {

    @Autowired
    private IUsuarioService usuarioSrv;

    @Autowired
    private ModelMapper modelMapper;

    @GetMapping
    public ResponseEntity<List<UsuarioDTO>> listar() {

        final List<UsuarioDTO> lista = usuarioSrv.listar().stream().map(user -> modelMapper.map(user, UsuarioDTO.class))
                .collect(Collectors.toList());
        if (lista.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return new ResponseEntity<List<UsuarioDTO>>(lista, HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<UsuarioDTO> registrar(@Valid @RequestBody final UsuarioDTO objeto) {

        final UsuarioDTO user = modelMapper.map(usuarioSrv.registrar(modelMapper.map(objeto, Usuario.class)),
                UsuarioDTO.class);
        final URI location = ServletUriComponentsBuilder.fromCurrentRequest().path("/{id}")
                .buildAndExpand(user.getIdUsuario()).toUri();
        return ResponseEntity.created(location).build();
    }

    @GetMapping("/{id}")
    public ResponseEntity<UsuarioDTO> listarPorId(@PathVariable("id") final Integer id) {
        final UsuarioDTO obj = modelMapper.map(usuarioSrv.listarPorId(id), UsuarioDTO.class);
        if (obj.getIdUsuario() == null) {
            throw new ModeloNotFoundException("ID NO ENCONTRADO " + id);
        }
        return new ResponseEntity<UsuarioDTO>(obj, HttpStatus.OK);
    }
}
