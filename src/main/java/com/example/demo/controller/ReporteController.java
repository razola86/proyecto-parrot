package com.example.demo.controller;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.ProductoDTO;
import com.example.demo.service.IProductoService;

@RestController
@RequestMapping("/reportes")
public class ReporteController {

    @Autowired
    private IProductoService productoSrv;

    @Autowired
    private ModelMapper modelMapper;

    private final String pat = "yyyy-MM-dd";
    private final SimpleDateFormat sdf = new SimpleDateFormat(pat);

    @GetMapping("/{fechaIni}/{fechaFin}")
    public ResponseEntity<List<ProductoDTO>> getReporte(
            @PathVariable("fechaIni") @DateTimeFormat(pattern = pat) final Date fechaIni,
            @PathVariable("fechaFin") @DateTimeFormat(pattern = pat) final Date fechaFin) {

        final List<ProductoDTO> lista = productoSrv.buscarReporte(sdf.format(fechaIni), sdf.format(fechaFin)).stream()
                .map(user -> modelMapper.map(user, ProductoDTO.class)).collect(Collectors.toList());

        final List<ProductoDTO> pdto = lista.stream().collect(Collectors
                .collectingAndThen(Collectors.toMap(ProductoDTO::getNombre, Function.identity(), (left, right) -> {
                    left.setCantidad(left.getCantidad() + right.getCantidad());
                    return left;
                }), m -> new ArrayList<>(m.values())));

        final List<ProductoDTO> listaOrden = pdto.stream()
                .sorted(Comparator.comparingInt(ProductoDTO::getCantidad).reversed()).collect(Collectors.toList());

        listaOrden.stream().forEach(p -> p.setTotal(p.getPrecioUnitario().multiply(new BigDecimal(p.getCantidad()))));
        if (listaOrden.isEmpty()) {
            return ResponseEntity.noContent().build();
        }
        return new ResponseEntity<List<ProductoDTO>>(listaOrden, HttpStatus.OK);
    }
}
