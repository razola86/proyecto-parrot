package com.example.demo.dto;

import java.io.Serializable;

import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.stereotype.Component;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "Informacion del Usuario")
@Component
public class UsuarioDTO implements Serializable {

    private static final long serialVersionUID = 4973966023294400402L;

    private Integer idUsuario;

    @Size(min = 3, max = 70, message = "El nombre debe contener al menos 3 caracteres y maximo 70")
    @NotNull(message = "Nombre no puede ser nulo")
    private String nombre;

    @Email(message = "Email debe ser valido")
    private String email;

    @AssertTrue(message = "Usuario debe ser activo")
    private boolean enabled;

    public Integer getIdUsuario() {
        return idUsuario;
    }

    public void setIdUsuario(final Integer idUsuario) {
        this.idUsuario = idUsuario;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(final String nombre) {
        this.nombre = nombre;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(final String email) {
        this.email = email;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(final boolean enabled) {
        this.enabled = enabled;
    }

}
