package com.example.demo.dto;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

import org.springframework.stereotype.Component;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description = "Informacion de la Orden")
@Component
public class OrdenDTO implements Serializable {

    private static final long serialVersionUID = -1372594180365274707L;

    private Integer idOrden;

    @ApiModelProperty(notes = "Referencia al Usuario que levanta la orden")
    @NotNull(message = "El usuario no debe ser Nulo")
    private UsuarioDTO usuario;

    @ApiModelProperty(notes = "Nombre del cliente minimo 3 caracteres")
    @Size(min = 3, max = 70, message = "El nombre debe contener al menos 3 caracteres y maximo 70")
    @NotNull(message = "El nombre no debe ser Nulo")
    private String nombreClinte;

    @ApiModelProperty(notes = "Total de la Orden, mayor a cero")
    @PositiveOrZero(message = "Total no puede ser menor a cero")
    private BigDecimal total;

    @JsonFormat(pattern = "dd/MM/yyyy HH:mm:ss")
    private LocalDateTime fecha;

    @NotEmpty(message = "Deber agregar Productos")
    private List<@Valid ProductoDTO> producto;

    public Integer getIdOrden() {
        return idOrden;
    }

    public void setIdOrden(final Integer idOrden) {
        this.idOrden = idOrden;
    }

    public UsuarioDTO getUsuario() {
        return usuario;
    }

    public void setUsuario(final UsuarioDTO usuario) {
        this.usuario = usuario;
    }

    public String getNombreClinte() {
        return nombreClinte;
    }

    public void setNombreClinte(final String nombreClinte) {
        this.nombreClinte = nombreClinte;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(final BigDecimal total) {
        this.total = total;
    }

    public LocalDateTime getFecha() {
        return fecha;
    }

    public void setFecha(final LocalDateTime fecha) {
        this.fecha = fecha;
    }

    public List<ProductoDTO> getProducto() {
        return producto;
    }

    public void setProducto(final List<ProductoDTO> producto) {
        this.producto = producto;
    }

}
