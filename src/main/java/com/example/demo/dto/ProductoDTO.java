package com.example.demo.dto;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;

import org.springframework.stereotype.Component;

import io.swagger.annotations.ApiModel;

@ApiModel(description = "Informacion del Producto")
@Component
public class ProductoDTO implements Serializable {

    private static final long serialVersionUID = -554848834523556560L;

    private Integer idProducto;

    @Size(min = 3, max = 70, message = "El nombre debe contener al menos 3 caracteres y maximo 70")
    @NotNull(message = "El nombre no debe ser Nulo")
    private String nombre;

    @PositiveOrZero(message = "Precio Unitario no puede ser menor a cero")
    private BigDecimal precioUnitario;

    @PositiveOrZero(message = "Cantidad no puede ser menor a cero")
    private Integer cantidad;

    private BigDecimal total;

    public Integer getIdProducto() {
        return idProducto;
    }

    public void setIdProducto(final Integer idProducto) {
        this.idProducto = idProducto;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(final String nombre) {
        this.nombre = nombre;
    }

    public BigDecimal getPrecioUnitario() {
        return precioUnitario;
    }

    public void setPrecioUnitario(final BigDecimal precioUnitario) {
        this.precioUnitario = precioUnitario;
    }

    public Integer getCantidad() {
        return cantidad;
    }

    public void setCantidad(final Integer cantidad) {
        this.cantidad = cantidad;
    }

    public BigDecimal getTotal() {
        return total;
    }

    public void setTotal(final BigDecimal total) {
        this.total = total;
    }

}
